package matrix;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

import matrix.task.DefaultTask;
import matrix.task.ExitTask;
import matrix.task.Task;
import matrix.task.impl.Task1Transpose;
import matrix.task.impl.Task2Addition;
import matrix.task.impl.Task3Multiplication;
import matrix.task.impl.Task4Vinograd;
import matrix.task.impl.Task5Strassen;

public class Starter {

    private static final Map<Integer, Task> NUMBER_TO_TASK;
    private static final Task DEFAULT_TASK = new DefaultTask();

    static {
        Map<Integer, Task> tasks = new LinkedHashMap<>();
        tasks.put(1, new Task1Transpose("task1"));
        tasks.put(2, new Task2Addition("task2"));
        tasks.put(3, new Task3Multiplication("task3"));
        tasks.put(4, new Task4Vinograd("task4"));
        tasks.put(5, new Task5Strassen("task5"));
        tasks.put(0, new ExitTask());

        NUMBER_TO_TASK = Collections.unmodifiableMap(tasks);
    }

    public static void main(String[] args) {
        while (true) {
            NUMBER_TO_TASK.forEach((number, task) -> System.out.println(number + ") " + task.getName()));
            System.out.print("Select: ");
            
            int select = readInt();
            NUMBER_TO_TASK.getOrDefault(select, DEFAULT_TASK).execute();
            System.out.println("\n\n");
        }
    }

    private static int readInt() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }
}
