package matrix.io;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import matrix.util.MatrixUtils;

public class TaskDataReader {

    private String folder;

    public TaskDataReader(String folder) {
        this.folder = folder;
    }

    private List<double[][]> getData() {
        try {
            URL resource = TaskDataReader.class.getClassLoader().getResource(folder);
            if (resource == null) {
                throw new RuntimeException("Folder \"" + folder + "\" is empty");
            }
            
            return Files.list(Paths.get(resource.toURI()))
                    .filter(path -> Files.isRegularFile(path))
                    .sorted(Comparator.comparing(file -> file.getFileName().toString()))
                    .map(this::read)
                    .collect(Collectors.toList());
        } catch (IOException | URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public List<double[][]> readMatrices(int count) {
        List<double[][]> data = getData();
        if (data.size() == count) {
            return data;
        } else {
            throw new RuntimeException("Too much files in folder \"" + folder + "\"");
        }
    }

    public double[][] readSingleMatrix() {
        return readMatrices(1).get(0);
    }

    private double[][] read(Path path) {
        try (BufferedReader reader = new BufferedReader(new FileReader(path.toFile()))) {
            List<double[]> rows = readNumbersMatrix(reader);
            MatrixUtils.assertValid(rows, path);
            
            double[][] result = new double[rows.size()][];
            for (int i = 0; i < result.length; i++) {
                result[i] = rows.get(i);
            }
            return result;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private List<double[]> readNumbersMatrix(BufferedReader reader) {
        return reader.lines()
                .filter(line -> !line.isEmpty())
                .map(line -> {
                    String[] numbers = line.split("\\s+");
                    double[] row = new double[numbers.length];

                    for (int i = 0; i < numbers.length; i++) {
                        row[i] = Double.parseDouble(numbers[i]);
                    }
                    return row;
                })
                .collect(Collectors.toList());
    }
}
