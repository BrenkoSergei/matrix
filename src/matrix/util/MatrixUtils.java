package matrix.util;

import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public final class MatrixUtils {

    private MatrixUtils() {
    }

    public static void display(double[][] a) {
        for (double[] row : a) {
            for (double value : row) {
                System.out.printf("%5.1f  ", value);
            }
            System.out.println();
        }
        System.out.println();
    }

    public static void assertValid(List<double[]> matrix, Path sourceFile) {
        assertEmptyList(matrix, "File \"" + sourceFile + "\" is empty");
        boolean isSameWidth = matrix.stream().allMatch(row -> row.length == matrix.get(0).length);
        if (!isSameWidth) {
            throw new RuntimeException("Rows in file \"" + sourceFile + "\" have different size");
        }
    }

    private static void assertEmptyList(List<?> rows, String message) {
        if (rows == null || rows.isEmpty()) {
            throw new RuntimeException(message);
        }
    }

    public static void assertDimensions(double[][] a, double[][] b) {
        if (a.length != b.length) {
            throw new RuntimeException("Matrices rows count are not the same");
        }
        Stream.concat(Arrays.stream(a), Arrays.stream(b))
                .forEach(doubles -> {
                    if (doubles.length != a[0].length) {
                        throw new RuntimeException("Matrices widths are not the same");
                    }
                });
    }

    public static void assertMultiplicationPossibility(double[][] a, double[][] b) {
        Arrays.stream(a)
                .forEach(doubles -> {
                    if (doubles.length != b.length) {
                        throw new RuntimeException("Matrices cannot be multiplied");
                    }
                });
    }
    
    public static double[][] multiply(double[][] a, double[][] b) {
        int rows_a = a.length;
        int columns_b = b[0].length;
        int columnsA_rowsB = a[0].length;

        double[][] result = new double[rows_a][columns_b];


        for (int i = 0; i < rows_a; i++) {
            for (int j = 0; j < columns_b; j++) {
                double sum = 0;
                for (int k = 0; k < columnsA_rowsB; k++) {
                    sum += a[i][k] * b[k][j];
                }
                result[i][j] = sum;
            }
        }
        return result;
    }
}
