package matrix.task;

public interface Task {

    void execute();
    
    String getName();
}
