package matrix.task.impl;

import java.util.List;

import matrix.util.MatrixUtils;

public class Task4Vinograd extends AbstractMatrixTask {

    public Task4Vinograd(String folder) {
        super(folder);
    }

    @Override
    public String getName() {
        return "Task 4 (Vinograd Multiplication)";
    }

    @Override
    protected void executeTask() {
        List<double[][]> matrices = dataReader.readMatrices(2);
        double[][] firstMatrix = matrices.get(0);
        double[][] secondMatrix = matrices.get(1);
        MatrixUtils.assertMultiplicationPossibility(firstMatrix, secondMatrix);

        System.out.println("First Matrix");
        MatrixUtils.display(firstMatrix);
        System.out.println("Second Matrix");
        MatrixUtils.display(secondMatrix);

        System.out.println("Result");
        MatrixUtils.display(multiplyVinograd(firstMatrix, secondMatrix));
    }

    private static double[][] multiplyVinograd(double[][] g, double[][] h) {
        int a = g.length;
        int b = h.length;
        int c = h[0].length;

        double[][] r = new double[a][c];


        int d = b / 2;

        double[] rowFactor = new double[a];
        double[] columnFactor = new double[c];

        for (int i = 0; i < a; i++) {
            rowFactor[i] = g[i][0] * g[i][1];
            for (int j = 1; j < d; j++) {
                rowFactor[i] += g[i][2 * j] * g[i][2 * j + 1];
            }
        }

        for (int i = 0; i < c; i++) {
            columnFactor[i] = h[0][i] * h[1][i];
            for (int j = 1; j < d; j++) {
                columnFactor[i] += h[2 * j][i] * h[2 * j + 1][i];
            }
        }

        for (int i = 0; i < a; i++) {
            for (int j = 0; j < c; j++) {
                r[i][j] = -rowFactor[i] - columnFactor[j];
                for (int k = 0; k < d; k++) {
                    r[i][j] += (g[i][2 * k] + h[2 * k + 1][j]) * (g[i][2 * k + 1] + h[2 * k][j]);
                }
            }
        }

        if (2 * (b / 2) != b) {
            for (int i = 0; i < a; i++) {
                for (int j = 0; j < c; j++) {
                    r[i][j] += g[i][b - 1] * h[b - 1][j];
                }
            }
        }

        return r;
    }
}
