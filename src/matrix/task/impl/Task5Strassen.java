package matrix.task.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import matrix.util.MatrixUtils;

public class Task5Strassen extends AbstractMatrixTask {

    public Task5Strassen(String folder) {
        super(folder);
    }

    @Override
    public String getName() {
        return "Task 5 (Strassen Multiplication)";
    }

    @Override
    protected void executeTask() {
        List<double[][]> matrices = dataReader.readMatrices(2);
        double[][] firstMatrix = matrices.get(0);
        double[][] secondMatrix = matrices.get(1);
        MatrixUtils.assertMultiplicationPossibility(firstMatrix, secondMatrix);

        System.out.println("First Matrix");
        MatrixUtils.display(firstMatrix);
        System.out.println("Second Matrix");
        MatrixUtils.display(secondMatrix);

        int dim = getNewDimension(firstMatrix, secondMatrix);
        double[][] g_n = extendMatrixToSquare2(firstMatrix, dim);
        double[][] h_n = extendMatrixToSquare2(secondMatrix, dim);

        double[][] r = multiplyStrassen(g_n, h_n, dim);


        System.out.println("Result");
        MatrixUtils.display(cutMatrix(r, firstMatrix.length, secondMatrix[0].length));
    }

    private static int getNewDimension(double[][] a, double[][] b) {
        return 1 << log2(Collections.max(Arrays.asList(a.length, a[0].length, b[0].length)));
    }

    private static int log2(int x) {
        int result = 1;
        while ((x >>= 1) != 0) {
            result++;
        }
        return result;
    }

    private static double[][] extendMatrixToSquare2(double[][] a, int n) {
        double[][] result = new double[n][n];

        for (int i = 0; i < a.length; i++) {
            System.arraycopy(a[i], 0, result[i], 0, a[i].length);
        }
        return result;
    }

    private static double[][] multiplyStrassen(double[][] a, double[][] b, int n) {
        if (n <= 4) {
            return MatrixUtils.multiply(a, b);
        }

        n = n >> 1;

        double[][] a11 = new double[n][n];
        double[][] a12 = new double[n][n];
        double[][] a21 = new double[n][n];
        double[][] a22 = new double[n][n];

        double[][] b11 = new double[n][n];
        double[][] b12 = new double[n][n];
        double[][] b21 = new double[n][n];
        double[][] b22 = new double[n][n];

        splitMatrix(a, a11, a12, a21, a22);
        splitMatrix(b, b11, b12, b21, b22);

        double[][] p1 = multiplyStrassen(sum(a11, a22), sum(b11, b22), n);
        double[][] p2 = multiplyStrassen(sum(a21, a22), b11, n);
        double[][] p3 = multiplyStrassen(a11, subtract(b12, b22), n);
        double[][] p4 = multiplyStrassen(a22, subtract(b21, b11), n);
        double[][] p5 = multiplyStrassen(sum(a11, a12), b22, n);
        double[][] p6 = multiplyStrassen(subtract(a21, a11), sum(b11, b12), n);
        double[][] p7 = multiplyStrassen(subtract(a12, a22), sum(b21, b22), n);

        double[][] c11 = sum(sum(p1, p4), subtract(p7, p5));
        double[][] c12 = sum(p3, p5);
        double[][] c21 = sum(p2, p4);
        double[][] c22 = sum(subtract(p1, p2), sum(p3, p6));

        return collectMatrix(c11, c12, c21, c22);
    }

    private static double[][] sum(double[][] a, double[][] b) {
        double[][] result = new double[a.length][a[0].length];

        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                result[i][j] = a[i][j] + b[i][j];
            }
        }
        return result;
    }

    private static double[][] subtract(double[][] a, double[][] b) {
        double[][] result = new double[a.length][a[0].length];

        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                result[i][j] = a[i][j] - b[i][j];
            }
        }
        return result;
    }
    
    private static void splitMatrix(double[][] a, double[][] a11, double[][] a12, double[][] a21, double[][] a22) {
        int n = a.length >> 1;

        for (int i = 0; i < n; i++) {
            System.arraycopy(a[i], 0, a11[i], 0, n);
            System.arraycopy(a[i], n, a12[i], 0, n);
            System.arraycopy(a[n + i], 0, a21[i], 0, n);
            System.arraycopy(a[n + i], n, a22[i], 0, n);
        }
    }

    private static double[][] collectMatrix(double[][] a11, double[][] a12, double[][] a21, double[][] a22) {
        int n = a11.length;
        double[][] a = new double[n << 1][n << 1];

        for (int i = 0; i < n; i++) {
            System.arraycopy(a11[i], 0, a[i], 0, n);
            System.arraycopy(a12[i], 0, a[i], n, n);
            System.arraycopy(a21[i], 0, a[n + i], 0, n);
            System.arraycopy(a22[i], 0, a[n + i], n, n);
        }
        return a;
    }

    private static double[][] cutMatrix(double[][] a, int rows, int columns) {
        double[][] result = new double[rows][columns];

        for (int i = 0; i < rows; i++) {
            System.arraycopy(a[i], 0, result[i], 0, columns);
        }

        return result;
    }
}
