package matrix.task.impl;

import matrix.io.TaskDataReader;
import matrix.task.Task;

public abstract class AbstractMatrixTask implements Task {

    TaskDataReader dataReader;

    AbstractMatrixTask(String folder) {
        this.dataReader = new TaskDataReader(folder);
    }

    @Override
    public void execute() {
        System.out.println("===== " + getName() + " Started =====");
        executeTask();
        System.out.println("===== " + getName() + " Finished =====");
    }

    protected abstract void executeTask();
}
