package matrix.task.impl;

import java.util.List;

import matrix.util.MatrixUtils;

public class Task3Multiplication extends AbstractMatrixTask {

    public Task3Multiplication(String folder) {
        super(folder);
    }

    @Override
    public String getName() {
        return "Task 3 (Matrix Multiplication)";
    }

    @Override
    protected void executeTask() {
        List<double[][]> matrices = dataReader.readMatrices(2);
        double[][] firstMatrix = matrices.get(0);
        double[][] secondMatrix = matrices.get(1);
        MatrixUtils.assertMultiplicationPossibility(firstMatrix, secondMatrix);

        System.out.println("First Matrix");
        MatrixUtils.display(firstMatrix);
        System.out.println("Second Matrix");
        MatrixUtils.display(secondMatrix);

        System.out.println("Result");
        MatrixUtils.display(MatrixUtils.multiply(firstMatrix, secondMatrix));
    }
}
