package matrix.task.impl;

import java.util.List;

import matrix.util.MatrixUtils;

public class Task2Addition extends AbstractMatrixTask {

    public Task2Addition(String folder) {
        super(folder);
    }

    @Override
    public String getName() {
        return "Task 2 (Matrix Addition)";
    }

    @Override
    protected void executeTask() {
        List<double[][]> matrices = dataReader.readMatrices(2);
        double[][] firstMatrix = matrices.get(0);
        double[][] secondMatrix = matrices.get(1);
        MatrixUtils.assertDimensions(firstMatrix, secondMatrix);

        System.out.println("First Matrix");
        MatrixUtils.display(firstMatrix);
        System.out.println("Second Matrix");
        MatrixUtils.display(secondMatrix);

        System.out.println("Result");
        MatrixUtils.display(sum(firstMatrix, secondMatrix));
    }

    private double[][] sum(double[][] a, double[][] b) {
        double[][] result = new double[a.length][a[0].length];

        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                result[i][j] = a[i][j] + b[i][j];
            }
        }
        return result;
    }
}
