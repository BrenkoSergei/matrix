package matrix.task.impl;

import matrix.util.MatrixUtils;

public class Task1Transpose extends AbstractMatrixTask {

    public Task1Transpose(String folder) {
        super(folder);
    }

    @Override
    public String getName() {
        return "Task 1 (Matrix Transpose)";
    }

    @Override
    protected void executeTask() {
        double[][] matrix = dataReader.readSingleMatrix();

        System.out.println("Input Data");
        MatrixUtils.display(matrix);
        
        transpose(matrix);
        
        System.out.println("Result");
        MatrixUtils.display(matrix);
    }

    private static void transpose(double[][] a) {
        int n = a.length;
        double temp;

        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                temp = a[i][j];
                a[i][j] = a[j][i];
                a[j][i] = temp;
            }
        }
    }
}
