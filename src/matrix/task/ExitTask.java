package matrix.task;

public class ExitTask implements Task {

    @Override
    public void execute() {
        System.exit(0);
    }

    @Override
    public String getName() {
        return "Exit";
    }
}
